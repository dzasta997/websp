var gameBox;
var points;
var colorLetter;
var isLetterInputAccepted;

function start() {
    gameBox = document.getElementById("gameBox");
    gameBox.addEventListener("mousemove", confirmGameStart, false);
    points = 0;
    colorLetter = '';
    isLetterInputAccepted = false;
}

function confirmGameStart() {
    let isGameStarted = confirm("Do you want to play?");
    if (isGameStarted) {
        beginGame();
    }
}

function beginGame() {
    gameBox.removeEventListener("mousemove", confirmGameStart, false);
    document.getElementById("textInput").addEventListener("input", onLetterInput, false);
    generateSquare("yellow");
    generateSquare("pink");
    generateSquare("green");
    generateSquare("magenta");
    generateSquare("orange");
}

function generateSquare(id) {
    let square = document.createElement("div");
    square.setAttribute("id", id);
    square.style.left = Math.floor(Math.random() * 353) + "px";
    square.style.top = Math.floor(Math.random() * 153) + "px";
    square.addEventListener("click", onSquareClick, false);
    gameBox.appendChild(square);
}

function onSquareClick(event) {
    var target = event.target;
    if (target.getAttribute("id") !== "yellow" && !event.ctrlKey) {
        colorLetter = target.getAttribute("id").charAt(0);
        isLetterInputAccepted = true;
        applyClickEffects(target);
    } else if (target.getAttribute("id") === "yellow" && event.ctrlKey) {
        applyClickEffects(target);
    }
}

function applyClickEffects(target) {
    target.remove();
    incrementPoints();
}

function onLetterInput(event) {
    let target = event.target;
    let letter = target.value;
    target.value = "";
    if (isLetterInputAccepted && colorLetter == letter) {
        incrementPoints();
    }
    isLetterInputAccepted = false;
}

function incrementPoints() {
    points++;
    let displayedPoints = document.getElementById("points");
    displayedPoints.innerHTML = points;
    if (points === 9) {
        printCompletonistMessage();
    }
}

function printCompletonistMessage() {
    let newNode = document.createTextNode("MAX number of points!");
    gameBox.parentNode.insertBefore(newNode, gameBox);
}

window.addEventListener("load", start, false);