function start() {
  var name = window.prompt("What is your name?", "Jim");
  var surname = window.prompt("What is your surname?", "Brown");
  var age = window.prompt("How old are you?", "31");
  var integerSum = promptInegerSum();
  var floatAverage = promptFloatAverage();

  printNavigation();
  printTitle();
  printPeronalData(name, surname, age);
  printIntegerSum(integerSum);
  printFloatAverage(floatAverage);
  printPowersOfIntegerSum(integerSum, 5);
  printSumOfFloatAverages(floatAverage, 20);
  printDots(integerSum, floatAverage);
}

function printNavigation() {
  document.writeln(
    '<header>' +
    '<nav>' +
    '<a href="index.html">Main</a> ' +
    '<a href="columns.html">Columns</a> ' +
    '<a href="form.html">Form</a> ' +
    '<a href="images.html">Images</a> ' +
    'Prompts ' +
    '<a href="references.html">References</a> ' +
    '<a href="table.html">Table</a> ' +
    '<a href="text.html">Text</a> ' +
    '</nav>' +
    '</header>'
  );
}

function printTitle() {
  document.writeln("<h1>Data</h1>");
}

function printPeronalData(name, surname, age) {
  document.writeln(
    "<p>Name: " + name + "</p>" +
    "<p>Surname: " + surname + "</p>" +
    "<p>Age: " + age + "</p>"
  );
}

function promptInegerSum() {
  var integerOne = parseInt(window.prompt("Enter integer (1/3)", "1"));
  var integertTwo = parseInt(window.prompt("Enter integer (2/3)", "1"));
  var integerThree = parseInt(window.prompt("Enter integer (3/3)?", "1"));
  return integerOne + integertTwo + integerThree;
}

function promptFloatAverage() {
  var floatOne = parseFloat(window.prompt("Enter float (1/3)", "1"));
  var floatTwo = parseFloat(window.prompt("Enter float (2/3)", "1"));
  var floatThree = parseFloat(window.prompt("Enter float (3/3)", "1"));
  return (floatOne + floatTwo + floatThree) / 3;
}

function printIntegerSum(sum) {
  document.writeln("<p>Integer sum: " + sum + "</p>");
}

function printFloatAverage(average) {
  document.writeln("<p>Float average: " + average + "</p>");
}

function printPowersOfIntegerSum(sum, exponent) {
  var paragraph = "<p>Integer sum powers until " + exponent + " exponent: 1";
  var power = 1;
  for (var i = 0; i < exponent; i++) {
    power *= sum;
    paragraph += ", " + power;
  }
  paragraph += ", ...</p>";
  document.writeln(paragraph);
}

function printSumOfFloatAverages(average, bound) {
  bound = Math.abs(bound);
  var sum = average;
  var paragraph = "<p>Float average sum until +/-" + bound + " is reached: ";
  if (average == 0) {
    paragraph += "average is equal to 0, so +/-" + bound + " is unreachable</p>";
  } else {
    paragraph += "0";
    while (-bound < sum && sum < bound) {
      paragraph += ", " + sum;
      sum += average;
    }
    paragraph += ", ...</p>";
  }
  document.writeln(paragraph);
}

function printDots(integerSum, floatAverage) {
  var paragraph = "<p>Print dots until |" + integerSum + " + floor(" + floatAverage + ")| is exceeded: "
  var bound = Math.abs(integerSum + Math.floor(floatAverage));
  do {
    paragraph += "."
    bound--;
  } while (bound > 0);
  paragraph += "</p>"
  document.writeln(paragraph);
}

window.addEventListener("load", start, false);