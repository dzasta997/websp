var generatedImage;
var generatedImageIdentifier;
var sameConsecutiveImageGenerationCount;

function start() {
  var button = document.getElementById("generateImageButton");
  button.addEventListener("click", generateImage, false);
  generatedImage = document.getElementById("generatedImage");
  generatedImageIdentifier = -1;
  previouslyGeneratedImageIdentifier = -1;
  sameConsecutiveImageGenerationCount = 0;
}

function generateImage() {
  var previouslyGeneratedImageIdentifier = generatedImageIdentifier;
  generatedImageIdentifier = generateImageIdentifier();
  sameConsecutiveImageGenerationCount = setGenerationCount(sameConsecutiveImageGenerationCount, generatedImageIdentifier, previouslyGeneratedImageIdentifier);
  setImage(generatedImage, generatedImageIdentifier);
  setStatusMessage(generatedImageIdentifier, sameConsecutiveImageGenerationCount);
}

function setImage(image, imageIdentifier) {
  switch (imageIdentifier) {
    case 0:
      setImageAttributes(image, "images/eagle-owl.png", "An image of an owl.");
      break;
    case 1:
      setImageAttributes(image, "images/flower.png", "An image of a flower.");
      break;
    case 2:
      setImageAttributes(image, "images/sun.png", "An image of the sun.");
      break;
  }
}

function setImageAttributes(imageToModify, src, alt) {
  imageToModify.setAttribute("src", src);
  imageToModify.setAttribute("alt", alt);
}

function generateImageIdentifier() {
  return Math.floor(Math.random() * 3);
}

function setGenerationCount(generationCount, identifier, previousIdentifier) {
  if (identifier != previousIdentifier) {
    return 1;
  } else {
    return generationCount + 1;
  }
}

function setStatusMessage(imageIdentifier, imageGenerationCount) {
  var statusMessageDiv = document.getElementById("generationStatusMessageDiv");
  var message = "<p>Generated an image of " + getImageName(imageIdentifier)
  if (imageGenerationCount == 1) {
    message += "!";
  } else {
    message += " " + imageGenerationCount + " times!";
  }
  message += "</p>";
  statusMessageDiv.innerHTML = message;
}

function getImageName(imageIdentifier) {
  switch (imageIdentifier) {
    case 0:
      return "an owl";
    case 1:
      return "a flower";
    case 2:
      return "the sun";
  }
}

window.addEventListener("load", start, false);