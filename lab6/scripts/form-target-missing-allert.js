function alertAboutMissingFormTarget() {
  window.alert("The form does not have a target to send the information to.");
}

window.addEventListener("load", alertAboutMissingFormTarget, false);