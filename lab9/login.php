<?php
if(isset($_COOKIE["mode"])){
//    print ("YES");
    $colorMode=$_COOKIE["mode"];
}
else{
    $colorMode="white-mode";
}

$passes = array("admin"=>"admin", "teacher"=>"password", "student"=>"123");

if(isset($_POST["login"])) {
    if (strcasecmp($_POST["login"], array_search($_POST["password"], $passes)) == 0) {
        print("true");
        session_start();
        $_SESSION["LOGIN"]=$_POST["login"];
        setcookie("sessionID", session_id());
        setcookie("login", $_SESSION["LOGIN"]);
        header("Refresh:0; url=index.php");
    } else {
        print ("Incorrect login or password");
    }
}

?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body class="<?php echo $colorMode ?>">
<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
    <label>Login:
    <input type="text" name="login">
    </label>
    <label>Password:
        <input type="password" name="password">
    </label>
    <input type="submit" value="Login">
</form>
<div>
    If you don't have an account yet, please register here:
    <button><a href="register.php">Register</a></button>
</div>
</body>
</html>
