<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>

<?php
const duration = 3600*24*31;

if(isset($_COOKIE["mode"])){
    //print ("YES");
    $colorMode=$_COOKIE["mode"];
}
else{
    $colorMode="white-mode";
}
?>

<body class="<?php echo $colorMode; ?>">

<?php
if(isset($_COOKIE["sessionID"])){
    $login = $_COOKIE["login"];
    echo "";
}
else{
    echo "<p>Login to see the content</p>
    <form  action='login.php'>
        <input type='submit' value='Login'>
    </form>";
    exit();
}


if(isset($_POST["mode"])){
    setcookie("mode", $_POST["mode"], time()+duration);
    $colorMode=$_COOKIE["mode"];
    header("Refresh:0");
}

?>
<a href="page.php">Hello</a>
<a href="db_list.php">Database</a>
<a href="register.php">Register/Edit</a>
<div style='display: flex; justify-content:space-between'>
<form method="post" action=<?php echo $_SERVER['PHP_SELF'] ?>>
    <label> Choose theme ->
        <select id="mode" name="mode">
            <option selected ><p><?php echo $colorMode ?></p></option>
            <option value="pink-mode">pink-mode</option>
            <option value="dark-mode">dark-mode</option>
            <option value="white-mode">white-mode</option>
            <option value="green-mode">green-mode</option>
            <option value="blue-mode">blue-mode</option>
        </select>
    </label>
    <input type="submit" value="Change">
</form>

    <form  action='logout.php'>
        <label>
            <?php echo $login?>
            <input type='submit' value='logout'>
        </label>

    </form>
</div>

<section>
    <h2>Table of contents</h2>
    <nav>
        <ul>
            <li><a href="#content">Content</a>
                <ul>
                    <li><a href="#editorial">Editorial</a>
                        <ul>
                            <li><a href="#articles">Articles</a>
                                <ul>
                                    <li>
                                        <a href="#loremipsum">Lorem ipsum dolor sit amet</a>
                                    </li>
                                    <li>
                                        <a href="#maurissit">Mauris sit amet convallis ligula</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="#editorsaddend">Editor's Addend</a></li>
                        </ul>
                    </li>
                    <li><a href="#faq">FAQ</a>
                        <ul>
                            <li><a href="#questions">Questions</a>
                                <ul>
                                    <li>
                                        <a href="#fusce">Fusce interdum hendrerit porta?</a>
                                    </li>
                                    <li>
                                        <a href="#aliquam">
                                            Aliquam consectetur auctor sapien ut feugiat?
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="#nunc">Nunc euismod</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
</section>
<section id="content">
    <h2>Content</h2>
    <section id="editorial">
        <h3>Editorial</h3>
        <article id="articles">
            <h4>Articles</h4>
            <article id="loremipsum">
                <h5>Lorem ipsum dolor sit amet</h5>
                <p>
                    Consectetur adipiscing elit. Aenean et urna sit amet sem iaculis dictum et ac tortor. Etiam rutrum metus ut
                    egestas feugiat. Nullam mattis nisi sit amet urna fringilla pellentesque vitae ac lacus. Donec placerat,
                    ante vitae tincidunt scelerisque, leo sapien aliquet nibh, in lacinia leo elit at elit. Aenean sollicitudin
                    orci justo, quis congue est porttitor ut. Cras at nisl lacus. Aenean in massa eget nulla tempor efficitur.
                    Vestibulum sed diam id odio facilisis mollis vel id velit. Cras non odio sit amet lacus blandit convallis a
                    ut sapien. Nam fringilla ex vitae magna aliquet egestas. Quisque quis consequat elit. Duis sodales tempus
                    augue ut bibendum. Morbi eu ante massa.
                </p>
                <p>
                    Cras non pellentesque magna. Suspendisse sodales quam id posuere blandit. Maecenas at lacus vitae arcu
                    iaculis varius. Aenean ligula magna, tristique vel finibus ac, cursus in quam. Suspendisse tincidunt ligula
                    ac nisl sagittis, ut ornare ipsum consequat. Morbi sollicitudin nunc tellus, eget sagittis ex volutpat
                    volutpat. Nunc in lorem dignissim, ornare dolor nec, cursus orci. Aliquam mi dolor, ultrices eu leo
                    efficitur, pharetra malesuada orci.
                </p>
            </article>
            <article id="maurissit">
                <h5>Mauris sit amet convallis ligula</h5>
                <p>
                    Nam in arcu eros. Curabitur ullamcorper viverra ullamcorper. Nulla aliquam quis neque lacinia feugiat. Ut in
                    egestas massa. Vestibulum rhoncus sit amet purus in gravida. Vestibulum vel velit nec nibh auctor dapibus
                    non id mauris. Nam elit velit, eleifend et rutrum sit amet, hendrerit id risus. Mauris id maximus lectus, at
                    tincidunt quam. Mauris finibus leo ut arcu tempor, vel accumsan sem ornare. Mauris nec lacus ultricies,
                    laoreet nulla eu, rutrum diam.
                </p>
                <p>
                    Aliquam nulla arcu, pharetra eget volutpat scelerisque, iaculis ac metus. Donec nec congue magna.
                    Suspendisse eleifend molestie eros, dapibus luctus sem mollis et. Nulla vel erat vestibulum, elementum
                    lectus ac, bibendum sapien. Vestibulum convallis augue sem, ac pulvinar sem aliquet eget. Sed aliquam, ante
                    eu iaculis ornare, mauris mauris pretium neque, at venenatis tortor nibh eget nulla. Duis non risus in nunc
                    congue aliquet vel ut augue. Nunc ultrices porta cursus. Nam in tellus vitae lacus fermentum gravida eget at
                    risus.
                </p>
                <p>
                    Nulla ultricies at mi vel sagittis. Suspendisse gravida nec est eget lacinia. Etiam in dui vitae nulla
                    feugiat dictum eu non tortor. Aenean placerat purus quis mauris finibus bibendum. Aenean consectetur egestas
                    metus vel sodales. Quisque euismod commodo nisi vitae hendrerit. Donec eu nisi dignissim, ornare orci
                    ornare, vestibulum felis. Donec tempor nibh metus, vel porttitor quam accumsan convallis. Sed elementum
                    bibendum dignissim. Sed sodales velit blandit augue bibendum aliquam. Proin tempus cursus odio non suscipit.
                    Nulla lobortis ut nibh sed pretium. Aliquam consequat lorem vitae nulla cursus pharetra. Curabitur vitae
                    ullamcorper ligula, tincidunt viverra mauris. Mauris convallis sodales consequat.
                </p>
            </article>
        </article>
        <article id="editorsaddend">
            <h4>Editor's Addend</h4>
            <p>
Curabitur quis dignissim arcu, tincidunt vestibulum ante. Fusce interdum hendrerit porta. Nulla facilisi.
Phasellus eget dui placerat, hendrerit sapien id, posuere ipsum. Aliquam ornare feugiat vulputate. Aenean
                egestas justo nisi, et lacinia ligula vulputate nec. Nullam lobortis lacus sed ipsum viverra, nec tincidunt
                magna ultricies. Nam viverra lobortis accumsan. Cras rutrum bibendum nisl eget faucibus.
            </p>
        </article>
    </section>
    <section id="faq">
        <h3>FAQ</h3>
        <article id="questions">
            <h4>Questions</h4>
            <article id="fusce">
                <h5>Fusce interdum hendrerit porta?</h5>
                <p>
Maecenas enim elit, auctor id urna eget, pellentesque vehicula massa. Praesent justo libero, auctor eu
                    libero quis, tempus eleifend dolor. Phasellus lacinia sit amet ante quis volutpat. Integer in nunc purus.
Etiam ultrices elementum quam, in sodales orci tincidunt ut. Cras lacus ipsum, auctor et odio quis,
                    scelerisque facilisis sapien.
                </p>
            </article>
            <article id="aliquam">
                <h5>Aliquam consectetur auctor sapien ut feugiat?</h5>
                <p>
Maecenas rutrum pharetra dui, non mattis leo facilisis vitae. In ac nisi viverra, pharetra sem eget,
                    tempor odio. Aliquam erat volutpat. Pellentesque eleifend dignissim orci ac venenatis. In vitae dui
                    mollis, placerat metus quis, dictum erat. Aliquam nec libero elit. Aenean quis sodales ligula. Etiam
                    euismod efficitur risus ac viverra. Donec laoreet consequat erat ut interdum. Pellentesque sapien nibh,
                    mollis eu ex vel, consequat vulputate risus. Integer massa justo, mattis quis leo sed, vulputate elementum
                    ipsum. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam sem
                    leo, rutrum eget justo at, aliquam rutrum velit.
                </p>
            </article>
        </article>
        <article id="nunc">
            <h4>Nunc euismod</h4>
            <p>
Ut vitae eros libero. Phasellus elit nunc, consequat a lacus sed, cursus accumsan dolor. Nunc luctus dolor at
                nisi congue fermentum.
            </p>
        </article>
    </section>
</section>
<form action="showCookies.php">
    <input type="submit" value="Show Cookies">
</form>
</body>
</html>
