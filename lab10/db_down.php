<?php
$db_host='localhost';
$db_user='root';
$db_password='root';
$link= mysqli_connect($db_host, $db_user, $db_password, "websp");
if(!$link){
    die("<p> Connection failed: ". mysqli_connect_error() ."<p>");
}
echo "<p> Connected successfully. </p>";
$query="DROP DATABASE websp";
if(mysqli_query($link, $query)){
    echo "Database removed";
}
else {
    echo "Error removing database: ".mysqli_error($link);
}
mysqli_close($link);
