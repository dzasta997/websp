<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>

<?php
const duration = 3600*24*31;

if(isset($_COOKIE["mode"])){
    //print ("YES");
    $colorMode=$_COOKIE["mode"];
}
else{
    $colorMode="white-mode";
}
?>

<body class="<?php echo $colorMode; ?>">

<?php
if(isset($_COOKIE["sessionID"])){
    $login = $_COOKIE["login"];
    echo "";
}
else{
    echo "<p>Login to see the content</p>
    <form  action='login.php'>
        <input type='submit' value='Login'>
    </form>";
    exit();
}


if(isset($_POST["mode"])){
    setcookie("mode", $_POST["mode"], time()+duration);
    $colorMode=$_COOKIE["mode"];
    header("Refresh:0");
}

?>
<a href="index.php">Home</a>
<div style='display: flex; justify-content:space-between'>
    <form method="post" action=<?php echo $_SERVER['PHP_SELF'] ?>>
        <label> Choose theme ->
            <select id="mode" name="mode">
                <option selected ><p><?php echo $colorMode ?></p></option>
                <option value="pink-mode">pink-mode</option>
                <option value="dark-mode">dark-mode</option>
                <option value="white-mode">white-mode</option>
                <option value="green-mode">green-mode</option>
                <option value="blue-mode">blue-mode</option>
            </select>
        </label>
        <input type="submit" value="Change">
    </form>

    <form  action='logout.php'>
        <label>
            <?php echo $login?>
            <input type='submit' value='logout'>
        </label>

    </form>
</div>
<h1>Hello <?php echo $login?></h1>
